import express from 'express'
import cors from 'cors'
import mongoose from 'mongoose'
import routes from './routes'
import chalk from 'chalk'
import path from 'path'

// import * as dotenv from "dotenv-safe"
// dotenv.config({ path: '../../.env'})

class App {

    public express: express.Application


    public constructor() {
        this.express = express()
        this.middlewares()
        this.database()
        this.routes()
    }

    private middlewares(): void {
        this.express.use(express.urlencoded({ extended: false }));
        this.express.use(express.json());
        this.express.use(cors());
        this.express.set("views", path.join(__dirname, "/pages"))
        this.express.set("view engine", "ejs")
        //this.express.use(express.static("/regulamentos/"));
    }

    

    private database(): void {
        
        mongoose.connect('mongodb://125.63.61.168:27017/gg-db', { useNewUrlParser: true, useCreateIndex: true, useUnifiedTopology: true }).then(() => {
            console.log(chalk.bgGreen("CONECTADO AO BANCO"))
        }).catch((err) => {
            console.log("ERRO -> " + err )
            console.log(chalk.bgRed("FALHA AO INICIAR O BANCO"))
            
        })
    }


    private routes() {
        this.express.use(routes)
    }

}


export const port = process.env.PORT || 3000
export default new App().express
