import { Request, Response } from 'express'
import path from 'path'
class GameController {

  
    public async getlistgames(req: Request, res: Response) {

        res.send({
            XONE: [
                { name: "FIFA 21", websocket_id: "FIFA21_XONE", cover: "fifa21" },
                { name: "RAINBOW SIX SIEGE", websocket_id: "R6SIEGE_XONE", cover: "r6siege" }
            ],
            PS4: [
                { name: "FIFA 21", websocket_id: "FIFA21_PS4", cover: "fifa21" },
                { name: "RAINBOW SIX SIEGE", websocket_id: "R6SIEGE_PS4", cover: "r6siege" }
            ],
            PC: [
                { name: "RAINBOW SIX SIEGE", websocket_id: "R6SIEGE_PC", cover: "r6siege" }
            ],
            CROSS: [
                { name: "CALL OF DUTY: WARZONE", websocket_id: "CODWARZONE_CROSS", cover: "codwarzone" }
            ],
        })

    }

    public async getcovers(req: Request, res: Response) {

        try {

            const pathcover = path.resolve(__dirname, "../static/covers")

            let covers = {
                "fifa21-cover": "https://i.ibb.co/kKZCgsL/fifa21-cover.jpg",
                "fifa21-banner": "https://i.ibb.co/NC6czW3/fifa21-banner.jpg",
                "fifa21-ultimate_team": "https://i.ibb.co/1vRDX0N/fifa21-banner-ultimate-team.jpg",

                "dota2-cover": "https://i.ibb.co/ypCZt94/dota2-cover.jpg",
                "dota2-banner": "https://i.ibb.co/4MCG4m1/dota2-banner.jpg",

                "codwarzone-cover": "https://i.ibb.co/hDY8p9k/codwarzone-cover.jpg",
                "codwarzone-banner": "https://i.ibb.co/Mf4SQmT/codwarzone-banner.jpg",

                "lol-cover": "https://i.ibb.co/4d0Rf5r/lol-cover.jpg",
                "lol-banner": "https://i.ibb.co/MV7gHfX/lol-banner.jpg",

                "nba21-cover": "https://i.ibb.co/XX1MMHZ/nba21-cover.jpg",
                "nba21-banner": "https://i.ibb.co/sFR4q3C/nba21-banner.jpg",

                "pes21-cover": "https://i.ibb.co/YZ5hVFV/pes21-cover.jpg",
                "pes21-banner": "https://i.ibb.co/dGqFVZ5/pes21-banner.jpg",

                "r6siege-cover": "https://i.ibb.co/zF7T9gp/r6siege-cover.jpg",
                "r6siege-banner": "https://i.ibb.co/6R6nPYt/r6siege-banner.jpg",

                "ufc4-cover": "https://i.ibb.co/y0h2x3N/ufc4-cover.jpg",
                "ufc4-banner": "https://i.ibb.co/Wty0HVt/ufc4-banner.jpg",

                "csgo-cover": "https://i.ibb.co/m82WLns/csgo-cover.jpg",
                "csgo-banner": "https://i.ibb.co/vHXHs3Y/csgo-banner.jpg"

            }

            res.status(200).json({

                "fifa21": {
                    cover: covers['fifa21-cover'],
                    banner: covers['fifa21-banner'],
                    banner_ultimate_team: covers['fifa21-ultimate_team']
                },

                "dota2": {
                    cover: covers['dota2-cover'],
                    banner: covers['dota2-banner']
                },

                "pes21": {
                    cover: covers['pes21-cover'],
                    banner: covers['pes21-banner']
                },

                "nba21": {
                    cover: covers['nba21-cover'],
                    banner: covers['nba21-banner']
                },

                "codwarzone": {
                    cover: covers['codwarzone-cover'],
                    banner: covers['codwarzone-banner']
                },

                "lol": {
                    cover: covers['lol-cover'],
                    banner: covers['lol-banner']
                },

                "ufc4": {
                    cover: covers['ufc4-cover'],
                    banner: covers['ufc4-banner']
                },

                "r6siege": {
                    cover: covers['r6siege-cover'],
                    banner: covers['r6siege-banner']
                },

                "csgo": {
                    cover: covers['csgo-cover'],
                    banner: covers['csgo-banner']
                }
            })

        } catch (error) {
            console.error(error)
            res.status(500).send("Falha no servidor")
        }
    }
}

export default new GameController