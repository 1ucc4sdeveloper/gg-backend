import { SHA256 } from "crypto-js";
import { NextFunction, Request, Response } from "express";
import jwt from "jsonwebtoken";
import Match from "../schemas/MatchSchema";

export default new class MatchController {
    public async inProgress(req: Request, res: Response) {
        const { user_id } = req.query;

        if (!user_id) return res.status(401).json({ msg: "user_id é obrigatório" });

        try {
            const matches = await Match.find({
                status: { $nin: ["canceled", "finished", "timeout", "waiting"] },
                users: { $elemMatch: { user_id: "6025de13d48a9300489b71c9" } },
            });

            return res
                .status(200)
                .json({
                    match: matches.map((match) => match?.toObject({ virtuals: true })),
                });
        } catch (err) {
            res
                .status(401)
                .send({ auth: false, msg: err.message || "Dados incorretos" });
        }
    }
}