import { Request, Response } from 'express'
import path from 'path'
import nodemailer from 'nodemailer'
import axios from 'axios'
import base64 from 'js-base64'
import chalk from 'chalk'
import Player from '../schemas/Player'

export default new class OthersController {

    public getnamebanks(req: Request, res: Response) {
        res.send({
            dealer: "Bruno Rodrigues",
            cpf: "401.605.028-61",
            banks: {
                bradesco: {
                    agency: "1689",
                    account: "0010298-9"
                },
                itau: {
                    agency: "9627",
                    account: "00585-4"
                },
                nubank: {
                    agency: "0001",
                    account: "41028161-8"

                },

                picpay: {
                    account: "@gg_br",
                },

                pix: {
                    account: "pix@goodgain.gg"
                }

            }
        })
    }

    public rules(req: Request, res: Response) {

        const pathrules = path.resolve(__dirname, "../pages/regulamentos/")

        res.sendFile(pathrules + "/ggranking/index.html")
    }

    public async sendMessage(req: Request, res: Response) {

        try {
            const mailtext = `Nome: ${req.body.name} E-mail: ${req.body.email} Assunto: ${req.body.subject} Mensagem: ${req.body.message} `;
            const mailhmtl = `<p><b>Nome: ${req.body.name} <br/> E-mail: ${req.body.email} <br/> Assunto: ${req.body.subject}</b></p> <br/> <p> Mensagem: ${req.body.message}</p>`;
            const to = "gg@goodgain.gg"
            const subject = "Mensagem enviada via site GoodGain"
            const text = mailtext
            const html = mailhmtl

            const transport = await nodemailer.createTransport({
                host: 'smtp.zoho.com.au',
                secure: true,
                port: 465,
                auth: {
                    user: 'gg@goodgain.gg',
                    pass: 'zTdbxjiPQsmj',
                },
            });

            const message = {
                replyTo: req.body.email,
                from: '"Good Gain International "<gg@goodgain.gg>',
                to,
                subject,
                text,
                html,
            };

            const response = transport.sendMail(message)
            return res.json({
                success: true,
                message: `Email enviado com sucesso!`,
            });
        } catch (e) {
            return res.status(401).send("Erro ao enviar email");
        };
    };

    public async pagSeguro(req: Request, res: Response){
        // req
        //{
        // pagseguro:{
        // 	"reference_id": "ex-00001",
        // 	"description": "Motivo da cobrança",
        // 	"amount": {
        // 	  "value": 1000,
        // 	  "currency": "BRL"
        // 	},
        // 	"payment_method": {
        // 	  "type": "CREDIT_CARD",
        // 	  "installments": 1,
        // 	  "capture": true,
        // 	  "card": {
        // 		"number": "4111111111111111",
        // 		"exp_month": "03",
        // 		"exp_year": "2026",
        // 		"security_code": "123",
        // 		"holder": {
        // 		  "name": "Jose da Silva"
        // 		}
        // 	  }
        // 	}
        // },
        // token:"token"
        //}
        try{
            var reqPag=req.body.pagseguro
		    reqPag.notification_urls=["http://goodgain.gg:3000/pagnotification/"]
            const response= await axios.post('https://api.pagseguro.com/charges',reqPag,{ headers: {
                'Authorization': '0cdfd113-2171-45dd-a3ec-7979bff6b5494c1b32ba484ab9a1f6f8f61c56f75d55f583-c110-4422-9854-c21bcd3621be',
                'x-api-version': '4.0'
            }})
            if(response.data.status=='DECLINED'){
                return res.json({status:false, data:response.data}) //retorna false se transação não aprovada
            }
    
            // cadastra infos transação
    
            return res.json({status:true, data:response.data}) //retorna true se não houve erros no cadastro
        
    }catch(e){
        console.log(e)
        return res.json({status:false, data:e})
        
    }
    
    };
    

    public myCash(req: Request, res: Response) {

        let data = req.body

        let { token } = data

        let tokenDecoded = base64.decode(token)

        let tokenEmail = tokenDecoded
            .trim()
            .replace('{"alg":"HS256","typ":"JWT"}{"email":"', "")
            .split(',')[0]
            .toString()
            .slice(0, -1)

        try {
            Player.findOne({ email: tokenEmail }).then((result: any) => {
                if (result.email == tokenEmail) {
                    console.log(chalk.gray("Solicitando saque"))
                    res.send({
                        last_date: "01/01/2021",
                        msg: "O Saque foi solicitado"
                    })
                }
            })
        } catch (error) {
            console.log("TOKEN INVALIDO")
            res.status(401).send("TOKEN INVALIDO / SAQUE INVALIDO")
        }



    }

    public infosocket(req: Request, res: Response) {

        let infos = {

            xone: {
                fifa21: {
                    pay: {
                        match: {
                            5: { receive: 8 },
                            15: { receive: 24 },
                            30: { receive: 48 }
                        },

                        tournament: {
                            15: { receive: 165 },
                            30: { receive: 330 },
                            50: { receive: 550 }
                        }
                    }
                },

                r6sige: {
                    pay: {
                        match: {
                            50: { receive: 80 }
                        },

                        tournament: {
                            100: { receive: 1100 }
                        }
                    }
                }
            },

            ps4: {
                fifa21: {
                    pay: {
                        match: {
                            5: { receive: 8 },
                            15: { receive: 24 },
                            30: { receive: 48 }
                        },

                        tournament: {
                            15: { receive: 165 },
                            30: { receive: 330 },
                            50: { receive: 550 }
                        }
                    }
                },

                r6sige: {
                    pay: {
                        match: {
                            50: { receive: 80 }
                        },

                        tournament: {
                            100: { receive: 1100 }
                        }
                    }
                }
            },

            pc: {
                r6sige: {
                    pay: {
                        match: {
                            50: { receive: 80 }
                        },

                        tournament: {
                            100: { receive: 1100 }
                        }
                    }
                }
            },

            cross: {
                codwarzone: {
                    pay: {
                        match: {
                            5: { receive: 8 }
                        },

                        tournament: {
                            10: { receive: 110 }
                        }
                    }
                }
            }

        }

        try {
            res.send(infos)
        } catch (error) {
            res.sendStatus(500).send("Falha no servidor")
        }


    }

}
