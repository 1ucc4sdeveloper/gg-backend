import { Request, Response, NextFunction } from 'express'
import Player, { PlayerInterface } from '../schemas/Player'
import chalk from 'chalk'
import jwt from 'jsonwebtoken'
import sha256 from 'crypto-js/sha256'
import base64 from 'js-base64'

class PlayerController {

    public async register(req: Request, res: Response) {

        let data = req.body


        //Criando jogador e validando conta 
        const newPlayer = new Player(data)
        newPlayer.save((err) => {

            if (err) {
                console.log(chalk.red("Cadastro INVALIDO ou JA CADASTRADO"))
                console.error(err)
                res.status(401).send("Cadastro invalido ! Campos incorretos ou usuario ja cadastrado")
            } else {

                // Retornar Status de Sucesso e cria uma carteira vazia para o novo jogador
                let { email } = data

                let secure_pass = sha256(data.pass).toString()


                Player.updateOne({ email: email }, { $set: { money: 0, XPrank: 0, bonus: 0, pass: secure_pass, doc: "", lastsocketid: "" } }, { upsert: true }).then(() => {
                    res.status(200).send("CADASTRADO COM SUCESSO")
                })

                console.log(chalk.green("CADASTRADO COM SUCESSO"))
            }
        })
    }

    public async login(req: Request, res: Response, next: NextFunction) {

        // Autenticação no Banco de dados
        // Devolve um JWT TOKEN apos autenticação
        let data = req.body

        let { email } = data

        let txt_pass = data.pass
        let pass = sha256(txt_pass).toString()

        try {

            await Player.findOne({ email: email }).then((result: any) => {

                if (result.email == email && result.pass == pass) {
                    const token = jwt.sign({ email }, "TOKEN", { expiresIn: "1d" })
                    console.log(chalk.green("USUÁRIO AUTENTICADO"))
                    res.send({
                        auth: true, token: token,
                        personaldata: {
                            name: result.name,
                            email: result.email,
                            nickname: result.nickname,
                            avatar: result.avatar,
                            XPrank: result.XPrank,
                            money: result.money,
                            bonus: result.bonus,
                            idonline: result.idvideogame,
                            id: result.id,
                            cpf: result.cpf,
                            doc: result.doc,
                            lastsocketid: result.lastsocketid
                        }
                    })
                }
            })

        } catch (error) {
            console.log(chalk.red("Falha na autenticação"))
            console.log(error)
            res.status(401).send("Senha ou Email invalidos")
        }

    }

    public async checkmoneyxp(req: Request, res: Response) {

        let data = req.body

        let { token } = data

        let tokenDecoded = base64.decode(token)

        let tokenEmail = tokenDecoded
            .trim()
            .replace('{"alg":"HS256","typ":"JWT"}{"email":"', "")
            .split(',')[0]
            .toString()
            .slice(0, -1)

        try {
            await Player.findOne({ email: tokenEmail }).then((result: any) => {
                if (result.email == tokenEmail) {
                    console.log(chalk.gray("Consultando Saldo e XP"))
                    res.send({
                        money: result.money,
                        XPrank: result.XPrank
                    })
                }
            })
        } catch (error) {
            console.log("TOKEN INVALIDO")
            res.status(401).send("TOKEN INVALIDO")
        }



    }


    //To FIX: Verificar se EMAIL Existe para nao criar um novo
    public async changelogin(req: Request, res: Response) {
        let data = req.body

        let { email } = data

        try {

            await Player.findOne({ email: email }).then((result: any) => {

                if (result.email == email) {
                    console.log("Email encontrado, enviando link")
                    res.status(200).send("Um Link foi enviado para o seu email")
                }
            })
        } catch (error) {
            res.status(401).send("Email nao encontrado")
        }



    }


    //To FIX: Verificar se EMAIL Existe para nao criar um novo
    public async uploadavatar(req: Request, res: Response) {

        let data = req.body

        let { token, avatarbase64 } = data

        let tokenDecoded = base64.decode(token)

        let tokenEmail = tokenDecoded
            .trim()
            .replace('{"alg":"HS256","typ":"JWT"}{"email":"', "")
            .split(',')[0]
            .toString()
            .slice(0, -1)

        Player.updateOne({ email: tokenEmail }, { $set: { avatar: avatarbase64 } }, { upsert: true }).then(() => {
            res.sendStatus(200)
        })
    }

    //To FIX: Verificar se EMAIL Existe para nao criar um novo
    public async updocument(req: Request, res: Response) {

        let data = req.body

        let { token, docbase64 } = data

        let tokenDecoded = base64.decode(token)

        let tokenEmail = tokenDecoded
            .trim()
            .replace('{"alg":"HS256","typ":"JWT"}{"email":"', "")
            .split(',')[0]
            .toString()
            .slice(0, -1)

        try {
            Player.updateOne({ email: tokenEmail }, { $set: { doc: docbase64 } }, { upsert: true }).then(() => {
                res.sendStatus(200)
            })
        } catch (error) {
            res.sendStatus(401).send("TOKEN INVALIDO ?")
        }


    }

    // Adicionar TRY CATCH
    public async vouncher(req: Request, res: Response) {

        let data = req.body

        let { token, code } = data

        let tokenDecoded = base64.decode(token)

        let tokenEmail = tokenDecoded
            .trim()
            .replace('{"alg":"HS256","typ":"JWT"}{"email":"', "")
            .split(',')[0]
            .toString()
            .slice(0, -1)


        let vouncher = code.toLowerCase()

        if (vouncher == "gg10") {
            Player.updateOne({ email: tokenEmail }, { $inc: { bonus: 10 } }, { upsert: true }).then(() => {
                res.sendStatus(200)
            })

            console.log("CUPOM GG10 ADICIONADO")

        } else {
            console.log("CUPOM INVALIDO")
            res.status(401).send("CUPOM INVALIDO")
        }


    }

}

export default new PlayerController()