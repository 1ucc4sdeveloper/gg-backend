import app from "./app";
import chalk, { bgMagenta } from "chalk";
import { port } from "./app";
import https from 'https';
import http from 'http'
import fs from 'fs';
import { initWebSocketServer } from './webSocketServer'

//const httpTesteServer = http.createServer(app).listen(4000, () => console.log(chalk.bgMagenta("HTTP na 4000")))
//const io = require("socket.io")(server);

 const key = fs.readFileSync('privkey.pem');
 const cert = fs.readFileSync('fullchain.pem');

 const server = https.createServer({
   key: fs.readFileSync('privkey.pem'),
   cert: fs.readFileSync('fullchain.pem'),
 }, app);


 server.listen(3000, () => console.log(chalk.white(`Servidor rodando na PORTA: ${port}`)));
//initWebSocketServer();

console.log(chalk.yellow(" ***** SERVER LOG ***** "));
