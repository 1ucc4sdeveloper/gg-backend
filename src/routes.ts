import { Router } from 'express'
import PlayerController from './controllers/PlayerController'
import GameController from './controllers/GameController'
import OthersController from './controllers/OthersController'
import MatchController from './controllers/MatchController'

const routes = Router()

// Players
routes.post('/register', PlayerController.register)
routes.post('/login', PlayerController.login)
routes.post('/checkmoneyxp', PlayerController.checkmoneyxp)
routes.post('/changelogin', PlayerController.changelogin)
routes.post('/uploadavatar', PlayerController.uploadavatar)
routes.post('/updocument', PlayerController.updocument)
routes.post('/vouncher', PlayerController.vouncher)

//Game
//routes.post('/registergame', GameController.registerGame)
routes.get('/getlistgames', GameController.getlistgames)
routes.get('/getcovers', GameController.getcovers)
//Others
routes.get('/infosocket', OthersController.infosocket)
routes.get('/getnamebanks', OthersController.getnamebanks)
routes.post('/contact', OthersController.sendMessage)
routes.post('/pagseguro', OthersController.pagSeguro)
routes.post('/mycash', OthersController.myCash)
//Regulamentos
routes.get('/rules', OthersController.rules)

//WEBSOCKET

routes.get("/match/in-progress", MatchController.inProgress);

export default routes
