import { Schema, model, Document } from 'mongoose'

export interface GameInterface extends Document {
    name?: String,
    videogame?: String,
    cover?: {
        data?: Buffer,
        type?: String
    }
}


const GameSchema = new Schema({

    name: {
        type: String,
        //required: true
    },

    videogame: {
        type: String,
        //required: true
    },

    cover: {
        type: Buffer,
        required: true
    }

})

export default model<GameInterface>('Game', GameSchema)