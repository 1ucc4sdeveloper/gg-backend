import { Schema, model, Document, VirtualType } from "mongoose";
import moment from "moment";
export interface MatchInterface {
  users?: any[];
  game?: string;
  platform?: string;
  value?: number;
  status?:
    | "waiting"
    | "match_inprogress"
    | "inprogress"
    | "finished"
    | "timeout"
    | "canceled";
  type?: "match" | "championship";
  createdAt?: Date;
  acceptedAt?: Date;
  acceptedDiff?: number;
  conectedAt?: Date;
  conectedDiff?: number;
  updatedAt?: Date;
}

export interface MatchInterfaceDoc extends MatchInterface, Document {}

const matchSchema = new Schema({
  users: {
    type: Array,
    required: true,
  },
  game: {
    type: String,
    required: true,
  },
  game_mode: {
    type: String,
    required: false,
  },
  platform: {
    type: String,
    required: true,
  },
  status: {
    type: String,
    required: true,
  },
  value: {
    type: Number,
    required: true,
  },
  type: {
    type: String,
    required: true,
  },
  createdAt: {
    type: Date,
    required: true,
  },
  acceptedAt: {
    type: Date,
    required: false,
  },
  conectedAt: {
    type: Date,
    required: false,
  },
  updatedAt: {
    type: Date,
    required: true,
  },
});

matchSchema.virtual("acceptedDiff").get(function () {
  return this.acceptedAt
    ? moment().diff(moment(this.acceptedAt), "seconds")
    : 0;
});

matchSchema.virtual("conectedDiff").get(function () {
  return this.conectedAt
    ? moment().diff(moment(this.conectedAt), "seconds")
    : 0;
});

export default model<MatchInterfaceDoc>("Match", matchSchema);
