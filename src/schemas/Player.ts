import { Schema, model, Document } from 'mongoose'

export interface PlayerInterface extends Document {
    name?: String
    email?: String
    country?: String
    state?: String
    pass?: String
    birthday?: Date
    sex?: String
    idvideogame?: Object
    money?: Number
    bonus?: Number
    XPrank?: Number
    id?: Number
    avatar?: String
    doc?: String
    cpf?: Number,
    nicknamegg: String,
    lastsocketid: String,
}

const playerSchema = new Schema({

    name: {
        type: String,
        required: true
    },
    email: {
        type: String,
        required: true,
        unique: true,
    },
    country: {
        type: String,
        required: true
    },

    state: {
        type: String,
        required: true
    },

    pass: {
        type: String,
        required: true
    },
    birthday: {
        type: Date,
        required: true
    },
    sex: {
        type: String,
        required: true,
        minlength: 1,
        maxlength: 1
    },

    /**
     * CPF deverá conter 11 Numeros,
     * Somente NUMERO Sem Digito,
     * Será no formato String para
     * evitar o erro da BASE OCTAL 
     * (Numeros que começam com Zero)
     */

    cpf: {
        type: String,
        required: true,
        minlength: 11,
        maxlength: 11
    },


    idonline: {
        type: Object,
        required: true
    },

    /**
     * CARACTERES VALIDOS: Letras minusculas e Numeros[0-9],
     * 
     * Min: 5 Characters,
     * Max : 15 Characters,
     * Nao é permitido espaço em branco
     * e/ou caracteres especiais
     * 
     *  * BACK-END NEGARÁ OS NICKS -> 
     * admin/admins/administrator/administrators/
     * ggadmin/admingg/administratorgg/ggadministrator/
     * support/supports/supportgg/ggsupport/ggofficial/officialgg
     * 
     * (Por razões de segurança, pois futuramente
     * terá como adicionar amigos)
     */

    nicknamegg: {
        type: String,
        required: true,
        unique: true,
        minlength: 5,
        maxlength: 15,
    },

    money: {
        type: Number
    },

    XPrank: {
        type: Number
    },

    bonus: {
        type: Number
    },

    avatar: {
        type: String
    },

    doc: {
        type: String
    },

})

export default model<PlayerInterface>('Player', playerSchema)