import http from "http";
import io from "socket.io";
import { v4 as generateUUID } from "uuid"
//import roomValues from "../configs/roomValues.json";
import { Express } from "express";
import chalk from "chalk";
import mongoose from "mongoose";
import Match, { MatchInterface } from "../src/schemas/MatchSchema";


class WebSocketServer {
  private server: io.Server;

  constructor(private express: Express, httpServer: http.Server) {
    var io = require("socket.io")
    this.server = io(httpServer);
    this.initSocket();
  }

  private initSocket(): void {
    console.log(chalk.green("✔ WebSocket Iniciado"));

    this.server.on("connection", (client: any) => {
      console.log(
        chalk.yellow(
          `Cliente conectado!: ${client.id} | clients: ${this.server.clients.length}`
        )
      );

      client.on("disconnect", () => {
        console.log(
          chalk.yellow(
            `disconnect: ${client.id} | clients: ${this.server.clients.length}`
          )
        );
      });

      client.on("findGame", async (props: any) => {
        console.log(chalk.magenta(`Valor passado: ${props.value}`));
        console.log(chalk.magenta(">> Procurando uma sala"));

        // if (!this.enoughCredit(value)) {
        //     console.log("✘ Crédito insuficiente para jogar")
        //     client.disconnect()
        //     return true
        // }

        console.log(chalk.magenta("✔ Vamos jogar!"));

        (await this.checkEmptyRoom(props, client))
          ? console.log("Ta vazio, criando")
          : console.log("Ta cheio, conectar");
      });

      client.on("send_challenge", async (props: any) => {
        const matchFinded = await Match.findOne({
          _id: props.match_id,
        });
        const match = matchFinded?.toObject();

        if (!match) {
          client.emit("Partida não encontrada");
          return;
        }

        match?.users?.forEach((user) => {
          if (user.user_id === props.user_id) user.challenge_accepted = true;
        });

        match.acceptedAt = new Date(); // define time to countdown in app

        this.server.to(match._id.toString()).emit("send_challenge", {
          msg: "Desafio aceito",
          match,
          user_id: props.user_id,
        });

        await Match.updateOne(
          { _id: new mongoose.Types.ObjectId(props.match_id) },
          match
        );
      });

      client.on("connection_created", async (props: any) => {
        const matchFinded = await Match.findOne({
          _id: props.match_id,
        });
        const match = matchFinded?.toObject();

        if (!match) {
          client.emit("Partida não encontrada");
          return;
        }

        match?.users?.forEach((user) => {
          if (user.user_id === props.user_id) user.connection_created = true;
        });

        match.acceptedAt = new Date(); // define time to countdown in app

        this.server.to(match._id.toString()).emit("connection_created", {
          msg: "Conexão criada",
          match,
          user_id: props.user_id,
        });

        await Match.updateOne(
          { _id: new mongoose.Types.ObjectId(props.match_id) },
          match
        );
      });

      client.on("match_online_created", async (props: any) => {
        const matchFinded = await Match.findOne({
          _id: props.match_id,
        });
        const match = matchFinded?.toObject();

        if (!match) {
          client.emit("Partida não encontrada");
          return;
        }

        match?.users?.forEach((user) => {
          if (user.user_id === props.user_id) user.match_online_created = true;
        });

        match.acceptedAt = new Date(); // define time to countdown in app

        this.server.to(match._id.toString()).emit("match_online_created", {
          msg: "Partida iniciada",
          match,
          user_id: props.user_id,
        });

        await Match.updateOne(
          { _id: new mongoose.Types.ObjectId(props.match_id) },
          match
        );
      });

      client.on("cancelFindGame", async (props: any) => {
        const matchFinded = await Match.findOne({
          _id: props.match_id,
        });
        const match = matchFinded?.toObject();

        if (!match) {
          client.emit("Partida não encontrada");
          return;
        }

        match.status = "canceled";
        await Match.updateOne(
          {
            _id: new mongoose.Types.ObjectId(props.match_id),
          },
          match
        );

        this.server.to(match._id.toString()).emit("send_challenge", {
          msg: "Desafio cancelado",
          match,
          user_id: props.user_id,
        });
      });
    });
  }

  private enoughCredit(value: Number): Boolean {
    if (value > 10) {
      console.log(">> Pode jogar");
      return true;
    } else {
      console.log(">> Nao pode jogar");
      return false;
    }
  }

  private async checkEmptyRoom(props: any, socket): any {
    let uuid = generateUUID();
    let platform = props.platform;
    let game = props.game;
    let value = props.value;
    let game_mode = props.game_mode;
    let token = props.token;
    let id = props.id;

    let objRoomsValues = JSON.parse(JSON.stringify(roomValues));

    //console.log(objRoomsValues[platform][game].match[value].wait.length)
    const match: any = {
      platform,
      game,
      value,
      game_mode,
      status: "waiting",
      type: "match",
    };

    const matchFinded = await Match.findOne({
      ...match,
    });

    if (!matchFinded) {
      console.log("vazia");
      const matchCreated = await Match.create({
        ...match,
        createdAt: new Date(),
        updatedAt: new Date(),
        users: [{ user_id: id, challenge_accepted: false }],
      });

      socket.join(matchCreated._id.toString());
      this.server.to(matchCreated._id.toString()).emit("findGame", {
        msg: "Partida criada",
        match: matchCreated,
        status: 1,
      });
      return true;
    } else {
      const match = { ...matchFinded.toObject() };
      match.users?.push({ user_id: id, challenge_accepted: false });
      match.status = "match_inprogress";
      match.updatedAt = new Date();

      await Match.updateOne({ _id: match._id }, match);
      socket.join(match._id.toString());
      // socket.emit("match", {
      //   msg: "Adversario encontrado",
      //   match: matchFinded,
      //   status: 2,
      // });
      this.server.to(match._id.toString()).emit("match", {
        msg: "Adversario encontrado",
        match: matchFinded,
        status: 2,
      });
      console.log("cheia");
      return false;
    }
  }
}

export { WebSocketServer };
